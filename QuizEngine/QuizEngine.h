//
//  QuizEngine.h
//  QuizEngine
//
//  Created by Miguel Mauricio Sierra on 12/01/21.
//  Copyright © 2021 Learning Miguel Sierra. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for QuizEngine.
FOUNDATION_EXPORT double QuizEngineVersionNumber;

//! Project version string for QuizEngine.
FOUNDATION_EXPORT const unsigned char QuizEngineVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <QuizEngine/PublicHeader.h>


