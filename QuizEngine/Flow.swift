//
//  Flow.swift
//  QuizEngine
//
//  Created by Miguel Mauricio Sierra on 12/01/21.
//  Copyright © 2021 Learning Miguel Sierra. All rights reserved.
//

import Foundation

//Abrir clase al lado del test ⇧(Shift)+⌥(Option) y click al archivo

protocol Router {
    func routeTo(question: String, answerCallback: @escaping (String) -> Void)
}

class Flow {
    let router: Router
    let questions: [String]
    
    init(questions: [String], router: Router) {
        self.questions = questions
        self.router = router
    }
    
    func start() {
        if let firstQuestion = questions.first {
            router.routeTo(question: firstQuestion) { [weak self] _ in //El router podría entregarle el closure a un tercero con lo cual se podría generar retain cycles, por lo cuál se deja una referencia weak y se utiliza un guard para no tener un crash en la app
                guard let strongSelf = self else { return }
                let firstQuestionIndex = strongSelf.questions.startIndex
                let secondQuestion = strongSelf.questions[firstQuestionIndex + 1]
                
                strongSelf.router.routeTo(question: secondQuestion) { _ in }
            }
        }
    }
}
